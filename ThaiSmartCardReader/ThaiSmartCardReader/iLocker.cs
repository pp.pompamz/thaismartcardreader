﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ThaiSmartCardReader
{
    public partial class iLocker : Form
    {
        [DllImport("user32.dll", CharSet = CharSet.Unicode)]
        static extern bool SetForegroundWindow(IntPtr hWnd);

        internal void Send(string dara)
        {
            Process[] procs = Process.GetProcessesByName("chrome");
            foreach (Process proc in procs)
            {
                if (proc.ProcessName == "chrome" || proc.ProcessName == "notepad" || proc.ProcessName == "firefox")
                {
                    SetForegroundWindow(proc.MainWindowHandle);
                    SendKeys.SendWait(dara);
                    SendKeys.SendWait("{TAB}");
                    break;
                }
            }
        }

    }
}
